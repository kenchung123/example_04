﻿using System.Collections.Generic;
using Example_04.Models;

namespace Example_04.Services
{
    public interface IGenericRepository<TEntity>
    {
        IEnumerable<TEntity> GetTEntity();
        TEntity GetById(int? id);
        void Insert(TEntity  tentity);
        void Delete(int id);
        void Update(TEntity tentity);
        void Save();
    }
}
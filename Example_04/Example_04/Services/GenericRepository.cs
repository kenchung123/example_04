﻿using System.Collections.Generic;
using Example_04.Models;

namespace Example_04.Services
{
    public class GenericRepository<TEntity> :IGenericRepository<TEntity> where TEntity : class
    {
        private readonly SChoolDbContext _contex;
        public GenericRepository(SChoolDbContext context)
        {
            _contex = context;
        }
        public IEnumerable<TEntity> GetTEntity()
        {
            return _contex.Set<TEntity>();
        }
        public TEntity GetById(int? id)
        {
            return _contex.Set<TEntity>().Find(id);     
        }
        public void Insert(TEntity tentity)
        {
            _contex.Set<TEntity>().Add(tentity);
            _contex.SaveChanges();
        }

        public void Delete(int id)
        {
            TEntity tentity = _contex.Set<TEntity>().Find(id);
            _contex.Set<TEntity>().Remove(tentity);
        }

        public void Update(TEntity tentity)
        {
            _contex.Set<TEntity>().Update(tentity);
            _contex.SaveChanges();
        }

        public void Save()
        {
            _contex.SaveChanges();
        }
    }
}